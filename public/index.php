<?php

use App\Flows\ExecuteOrder\CreateOrder;
use App\Flows\ExecuteOrder\FilterWithoutVipOrders;
use App\Flows\ExecuteOrder\Success;
use App\Flows\ExecuteOrder\TryToReserve;
use App\States\StateForApi;
use App\States\StateForApiCurrentVip;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

$app = require __DIR__ . '/../app/bootstrap.php';

$app->get('/', function (Request $request, Response $response) {
    $response->getBody()->write("Hello world!");
    return $response;
});

// метод должен быть post, для удобства тестирования из браузера сделал get
$app->get('/order', function (Request $request, Response $response, $args) {
    $query = $request->getQueryParams();

    if ($query['current_vip'] ?? '0' === '1') {
        $state = $this->get(StateForApiCurrentVip::class)->make();
    } else {
        $state = $this->get(StateForApi::class)->make();
    }

    $firstStepFlow = $flow = $this->get(CreateOrder::class);

    if ($state['current_customer']->isVip()) {
        $flow = $flow->setNext($this->get(FilterWithoutVipOrders::class));
    }

    $flow
        ->setNext($this->get(TryToReserve::class))
        ->setNext($this->get(Success::class));

    $result = $firstStepFlow(
        $query['begin'],
        $query['end'],
        $state['current_customer'],
        $state['craftsman']
    );

    $response->getBody()->write(json_encode([
        'success' => $result->isSuccess(),
        'message' => $result->getMessage()
    ], JSON_THROW_ON_ERROR | JSON_UNESCAPED_UNICODE));

    $response = $response->withHeader('Content-Type', 'application/json');

    if (!$result->isSuccess()) {
        $response = $response->withStatus(400);
    }

    return $response;
});

$app->run();