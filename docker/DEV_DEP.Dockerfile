FROM composer:latest AS composer
FROM php:7.4-cli

ARG REDIS_EXT_VERSION="5.3.1"

RUN apt-get update && apt-get install -y \
        libpq-dev \
        libzip-dev \
        zip \
    && docker-php-ext-configure pgsql -with-pgsql=/usr/local/pgsql \
    && docker-php-ext-install pdo pdo_pgsql pgsql \
    && docker-php-ext-install zip \
    # redis
    && pecl install -o -f redis \
    &&  echo "extension=redis.so" > /usr/local/etc/php/conf.d/redis.ini \
    # remove
    &&  rm -rf /tmp/pear \
    && apt-get remove -y \
        libpq-dev \
        libzip-dev

RUN mkdir /app
WORKDIR /app

COPY --from=composer /usr/bin/composer /usr/bin/composer
COPY ./composer.json /app/composer.json
RUN composer install --prefer-dist --no-scripts --no-autoloader && rm -rf /root/.composer

COPY . /app

RUN composer dump-autoload --no-scripts --optimize

EXPOSE 8000
CMD [ "php", "-S", "0.0.0.0:8000", "-t", "public" ]
