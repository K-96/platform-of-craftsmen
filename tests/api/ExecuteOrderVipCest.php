<?php 

class ExecuteOrderVipCest
{
    public function createOrder(ApiTester $I)
    {
        $I->sendGet('/order', [
            'begin' => '2020-09-17 08:45:00',
            'end' => '2020-09-17 09:33:00',
            'current_vip' => '1',
        ]);
        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson(['success' => true]);
    }
}
