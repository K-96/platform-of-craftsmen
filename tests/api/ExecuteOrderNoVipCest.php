<?php 

class ExecuteOrderNoVipCest
{
    public function createOrder(ApiTester $I)
    {
        $I->sendGet('/order', [
            'begin' => '2020-09-17 08:45:00',
            'end' => '2020-09-17 09:33:00',
        ]);
        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::BAD_REQUEST);
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson(['success' => false]);
    }
}
