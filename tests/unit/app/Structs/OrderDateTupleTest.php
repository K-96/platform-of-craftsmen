<?php

namespace app\Structs;

use App\Structs\OrderDateTuple;
use DateTime;

class OrderDateTupleTest extends \Codeception\Test\Unit
{
    public function testIntersect()
    {
        $tuple1 = new OrderDateTuple(
            new DateTime('2020-09-20 16:00:00'),
            new DateTime('2020-09-20 20:00:00')
        );
        $tuple2 = new OrderDateTuple(
            new DateTime('2020-09-20 20:00:00'),
            new DateTime('2020-09-20 21:00:00')
        );
        $tuple3 = new OrderDateTuple(
            new DateTime('2020-09-20 19:59:59'),
            new DateTime('2020-09-20 21:00:00')
        );

        verify($tuple1->intersect($tuple2))
            ->equals(false, 'Даты пересекаются в 20:00:00, но мы не проверяем включительно');
        verify($tuple2->intersect($tuple1))
            ->equals(false, 'Проверка через второй объект.');

        verify($tuple1->intersect($tuple3))
            ->equals(true, 'Даты пересекаются в 19:59:59');
        verify($tuple3->intersect($tuple1))
            ->equals(true, 'Проверка через второй объект.');
    }
}