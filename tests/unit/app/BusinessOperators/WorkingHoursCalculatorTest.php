<?php

namespace app\BusinessOperators;

use App\BusinessOperators\NormalizingWorkTime;
use App\BusinessOperators\WorkingHoursCalculator;
use DateTime;

class WorkingHoursCalculatorTest extends \Codeception\Test\Unit
{
    public function testCalculate()
    {
        $normalizingWorkTime = new NormalizingWorkTime(0, 16);
        $workingHoursCalculator = new WorkingHoursCalculator($normalizingWorkTime);

        verify(
            $workingHoursCalculator->calculate(
                $normalizingWorkTime->normalizeBegin(new DateTime('2020-09-10 00:00:00')),
                $normalizingWorkTime->normalizeEnd(new DateTime('2020-09-15 21:32:25'))
            )
        )->equals(6 * 16, 'Заказ на 6 полных дней.');
        verify(
            $workingHoursCalculator->calculate(
                $normalizingWorkTime->normalizeBegin(new DateTime('2020-09-15 15:00:00')),
                $normalizingWorkTime->normalizeEnd(new DateTime('2020-09-15 21:32:25'))
            )
        )->equals(1, 'Заказ на 1 час.');
        verify(
            $workingHoursCalculator->calculate(
                $normalizingWorkTime->normalizeBegin(new DateTime('2020-09-15 09:33:56')),
                $normalizingWorkTime->normalizeEnd(new DateTime('2020-09-16 10:32:25'))
            )
        )->equals(7 + 11, 'Заказ на 18 часов.');
    }
}