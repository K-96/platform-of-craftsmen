<?php

namespace app\BusinessOperators;

use App\BusinessOperators\NormalizingWorkTime;
use DateTime;

class NormalizingWorkTimeTest extends \Codeception\Test\Unit
{
    public function testBegin()
    {
        $normalizingWorkTime = new NormalizingWorkTime(0, 16);

        verify(
            $normalizingWorkTime
                ->normalizeBegin(new DateTime('2020-09-20 13:00:00'))
                ->format('Y-m-d H:i:s')
        )->equals('2020-09-20 13:00:00', 'Дата не должна поменяться');
        verify(
            $normalizingWorkTime
                ->normalizeBegin(new DateTime('2020-09-20 13:00:01'))
                ->format('Y-m-d H:i:s')
        )->equals('2020-09-20 13:00:00', 'Дата должна округлится вниз');
        verify(
            $normalizingWorkTime
                ->normalizeBegin(new DateTime('2020-09-20 13:33:29'))
                ->format('Y-m-d H:i:s')
        )->equals('2020-09-20 13:00:00', 'Дата должна округлится вниз');
        verify(
            $normalizingWorkTime
                ->normalizeBegin(new DateTime('2020-09-20 13:59:59'))
                ->format('Y-m-d H:i:s')
        )->equals('2020-09-20 13:00:00', 'Дата должна округлится вниз');
        verify(
            $normalizingWorkTime
                ->normalizeBegin(new DateTime('2020-09-20 14:00:00'))
                ->format('Y-m-d H:i:s')
        )->equals('2020-09-20 14:00:00', 'Дата не должна поменяться');
    }

    public function testEnd()
    {
        $normalizingWorkTime = new NormalizingWorkTime(0, 16);

        verify(
            $normalizingWorkTime
                ->normalizeEnd(new DateTime('2020-09-20 14:00:00'))
                ->format('Y-m-d H:i:s')
        )->equals('2020-09-20 14:00:00', 'Дата не должна поменяться');
        verify(
            $normalizingWorkTime
                ->normalizeEnd(new DateTime('2020-09-20 13:59:59'))
                ->format('Y-m-d H:i:s')
        )->equals('2020-09-20 14:00:00', 'Дата должна округлится верх');
        verify(
            $normalizingWorkTime
                ->normalizeEnd(new DateTime('2020-09-20 13:11:32'))
                ->format('Y-m-d H:i:s')
        )->equals('2020-09-20 14:00:00', 'Дата должна округлится верх');
        verify(
            $normalizingWorkTime
                ->normalizeEnd(new DateTime('2020-09-20 13:00:01'))
                ->format('Y-m-d H:i:s')
        )->equals('2020-09-20 14:00:00', 'Дата должна округлится верх');
        verify(
            $normalizingWorkTime
                ->normalizeEnd(new DateTime('2020-09-20 13:00:00'))
                ->format('Y-m-d H:i:s')
        )->equals('2020-09-20 13:00:00', 'Дата не должна поменяться');
    }

    public function testAmountHoursInWorkday()
    {
        $normalizingWorkTime = new NormalizingWorkTime(0, 16);

        verify($normalizingWorkTime->getNumberOfWorkingHoursPerDay())
            ->equals(16, 'Должно быть 16 часов');

        $normalizingWorkTime = new NormalizingWorkTime(9, 18);

        verify($normalizingWorkTime->getNumberOfWorkingHoursPerDay())
            ->equals(9, 'Должно быть 9 часов');
    }

    public function testSliceOfExtraHours()
    {
        $normalizingWorkTime = new NormalizingWorkTime(9, 18);

        verify(
            $normalizingWorkTime
                ->normalizeBegin(new DateTime('2020-09-20 03:33:44'))
                ->format('Y-m-d H:i:s')
        )->equals('2020-09-20 09:00:00', 'Время должно быть равно 9 часам');
        verify(
            $normalizingWorkTime
                ->normalizeEnd(new DateTime('2020-09-20 19:33:44'))
                ->format('Y-m-d H:i:s')
        )->equals('2020-09-20 18:00:00', 'Время должно быть равно 18 часам');
    }
}