<?php

use Slim\Factory\AppFactory;
use App\BusinessOperators\NormalizingWorkTime;
use DI\Container;

require __DIR__ . '/../vendor/autoload.php';

$container = new Container();
AppFactory::setContainer($container);

$app = AppFactory::create();

$container->set(NormalizingWorkTime::class, function () {
    return new NormalizingWorkTime(0, 16);
});

return $app;
