<?php

namespace App\States;

use App\BusinessOperators\NormalizingWorkTime;
use App\Models\Customer;
use App\Models\Order;
use App\Structs\OrderDateTuple;
use DateTime;

class StateForApi extends State
{
    private NormalizingWorkTime $normalizingWorkTime;

    public function __construct(NormalizingWorkTime $normalizingWorkTime)
    {
        $this->normalizingWorkTime = $normalizingWorkTime;
    }

    public function make(): array
    {
        $state = parent::make();

        $customer1 = new Customer();
        $order1 = $this->makeOrder('2020-09-17 09:11:00', '2020-09-17 11:54:00', $customer1);
        $order2 = $this->makeOrder('2020-09-17 12:34:00', '2020-09-17 14:34:00', $customer1);

        $customer2 = new Customer();
        $customer2->upgradeToVip();
        $order3 = $this->makeOrder('2020-09-17 15:22:00', '2020-09-17 17:34:00', $customer2);
        $order4 = $this->makeOrder('2020-09-18 00:00:00', '2020-09-20 18:34:00', $customer2);
        $order5 = $this->makeOrder('2020-09-21 00:00:00', '2020-09-21 01:34:00', $customer2);

        $customer3 = new Customer();
        $order6 = $this->makeOrder('2020-09-21 02:00:00', '2020-09-21 03:00:00', $customer3);
        $order7 = $this->makeOrder('2020-09-21 09:00:00', '2020-09-21 10:34:00', $customer3);

        $state['craftsman']
            ->addOrder($order1)
            ->addOrder($order2)
            ->addOrder($order3)
            ->addOrder($order4)
            ->addOrder($order5)
            ->addOrder($order6)
            ->addOrder($order7);

        return $state;
    }

    private function makeOrder(string $begin, string $end, Customer $customer): Order
    {
        $begin = $this->normalizingWorkTime->normalizeBegin(new DateTime($begin));
        $end = $this->normalizingWorkTime->normalizeEnd(new DateTime($end));

        return new Order(
            new OrderDateTuple($begin, $end),
            $customer
        );
    }
}