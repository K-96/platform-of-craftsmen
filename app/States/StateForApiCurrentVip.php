<?php

namespace App\States;


class StateForApiCurrentVip extends State
{
    private StateForApi $stateForApi;

    public function __construct(StateForApi $stateForApi)
    {
        $this->stateForApi = $stateForApi;
    }

    public function make(): array
    {
        $state = $this->stateForApi->make();

        $state['current_customer']->upgradeToVip();

        return $state;
    }

}