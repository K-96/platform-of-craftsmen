<?php

namespace App\States;

use App\Models\Craftsman;
use App\Models\Customer;

abstract class State
{
    protected function make(): array
    {
        return [
            'craftsman' => new Craftsman(),
            'current_customer' => new Customer(),
        ];
    }
}