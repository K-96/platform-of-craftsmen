<?php

namespace App\BusinessOperators;

use DateTime;
use DateInterval;

class NormalizingWorkTime
{
    private int $beginOfWorkday;
    private int $endOfWorkday;

    public function __construct(int $beginOfWorkday, int $endOfWorkday)
    {
        $this->beginOfWorkday = $beginOfWorkday;
        $this->endOfWorkday = $endOfWorkday;
    }

    public function getNumberOfWorkingHoursPerDay(): int
    {
        return $this->endOfWorkday - $this->beginOfWorkday;
    }

    public function normalizeBegin(DateTime $date): DateTime
    {
        $date = $this->resetMinutesAndSeconds($date);

        return $this->normalize($date);
    }

    public function normalizeEnd(DateTime $date): DateTime
    {
        $date = $this->tryShiftUntilNextHour($date);
        $date = $this->resetMinutesAndSeconds($date);

        return $this->normalize($date);
    }

    private function normalize(DateTime $date): DateTime
    {
        $hours = (int) $date->format('H');

        if ($hours < $this->beginOfWorkday) {
            $date = $this->makeBeginTime($date);
        } else if ($hours > $this->endOfWorkday) {
            $date = $this->makeEndTime($date);
        }

        return $date;
    }

    private function makeBeginTime(DateTime $date): DateTime
    {
        return $date->setTime($this->beginOfWorkday, 0, 0);
    }

    private function makeEndTime(DateTime $date): DateTime
    {
        return $date->setTime($this->endOfWorkday, 0, 0);
    }

    private function tryShiftUntilNextHour(DateTime $date): DateTime
    {
        if ((int) $date->format('i') !== 0 || (int) $date->format('s') !== 0) {
            return $date->add(new DateInterval('PT1H'));
        }

        return $date;
    }

    private function resetMinutesAndSeconds(DateTime $date): DateTime
    {
        if ((int) $date->format('i') !== 0 || (int) $date->format('s') !== 0) {
            return $date->setTime((int) $date->format('H'), 0, 0);
        }

        return $date;
    }
}