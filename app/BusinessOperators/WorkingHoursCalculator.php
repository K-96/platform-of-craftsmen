<?php

namespace App\BusinessOperators;

use DateTime;

class WorkingHoursCalculator
{
    private NormalizingWorkTime $normalizingWorkTime;

    public function __construct(NormalizingWorkTime $normalizingWorkTime)
    {
        $this->normalizingWorkTime = $normalizingWorkTime;
    }

    public function calculate(DateTime $begin, DateTime $end): int
    {
        $diff = $end->diff($begin);

        return $diff->d * $this->normalizingWorkTime->getNumberOfWorkingHoursPerDay() + $diff->h;
    }
}