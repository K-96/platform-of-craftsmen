<?php

namespace App\Models;

class Craftsman extends Model
{
    /** @var Order[] */
    private array $orders = [];

    public function addOrder(Order $order): self
    {
        $this->orders[] = $order;

        return $this;
    }

    /**
     * @return Order[]
     */
    public function getOrders(): array
    {
        return $this->orders;
    }
}