<?php


namespace App\Models;


class Customer extends Model
{
    private bool $vip = false;

    public function upgradeToVip(): void
    {
        $this->vip = true;
    }

    public function isVip(): bool
    {
        return $this->vip;
    }
}