<?php

namespace App\Models;

use Exception;

abstract class Model
{
    private int $id;

    public function __construct()
    {
        try {
            $this->id = random_int(100_000, 999_999);
        } catch (\Exception $e) {
            $this->id = 100_000;
        }
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function save(): bool
    {
        return false;
    }
}