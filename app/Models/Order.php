<?php

namespace App\Models;

use App\Structs\OrderDateTuple;

class Order extends Model
{
    private Customer $customer;
    private OrderDateTuple $dates;

    public function __construct(OrderDateTuple $dates, Customer $customer)
    {
        parent::__construct();

        $this->dates = $dates;
        $this->customer = $customer;
    }

    public function getDates(): OrderDateTuple
    {
        return $this->dates;
    }

    public function getCustomer(): Customer
    {
        return $this->customer;
    }
}