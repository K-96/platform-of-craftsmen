<?php

namespace App\Flows;

use Throwable;

class Result
{
    private string $status;
    private string $message;
    private ?Throwable $exception = null;

    public function __construct()
    {
        $this->status = 'none';
    }

    public function success(): self
    {
        $this->status = 'success';
        return $this;
    }

    public function error(): self
    {
        $this->status = 'error';
        return $this;
    }

    public function message(string $message): self
    {
        $this->message = $message;
        return $this;
    }

    public function exception(Throwable $exception): self
    {
        $this->exception = $exception;

        $this->message($this->exception->getMessage());
        $this->error();

        return $this;
    }

    public function isSuccess(): bool
    {
        return $this->status === 'success';
    }

    public function getStatus(): string
    {
        return $this->status;
    }

    public function getMessage(): string
    {
        return $this->message;
    }

    public function getException(): ?Throwable
    {
        return $this->exception;
    }
}