<?php

namespace App\Flows;

interface Step
{
    public function setNext(self $next): self;
}