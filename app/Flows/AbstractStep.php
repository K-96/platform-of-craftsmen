<?php

namespace App\Flows;

use Closure;

abstract class AbstractStep implements Step
{
    protected ?Step $next = null;

    public function setNext(Step $next): self
    {
        $this->next = $next;
        return $next;
    }
}