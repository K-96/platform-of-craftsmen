<?php

namespace App\Flows\ExecuteOrder;

use App\BusinessOperators\NormalizingWorkTime;
use App\Flows\AbstractStep;
use App\Flows\Result;
use App\Models\Craftsman;
use App\Models\Customer;
use App\Models\Order;
use App\Structs\OrderDateTuple;
use DateTime;

class CreateOrder extends AbstractStep
{
    private NormalizingWorkTime $normalizingWorkTime;

    public function __construct(NormalizingWorkTime $normalizingWorkTime)
    {
        $this->normalizingWorkTime = $normalizingWorkTime;
    }

    public function __invoke(
        string $begin,
        string $end,
        Customer $currentCustomer,
        Craftsman $craftsman
    ): Result
    {
        $beginDateTime = $this->normalizingWorkTime->normalizeBegin(new DateTime($begin));
        $endDateTime = $this->normalizingWorkTime->normalizeEnd(new DateTime($end));

        $order = new Order(new OrderDateTuple($beginDateTime, $endDateTime), $currentCustomer);

        return ($this->next)($order, $craftsman->getOrders());
    }
}