<?php

namespace App\Flows\ExecuteOrder;

use App\Flows\AbstractStep;
use App\Flows\Result;
use App\Flows\StopFlow;
use App\Models\Order;

class TryToReserve extends AbstractStep
{
    /**
     * @param Order $currentOrder
     * @param Order[] $reservedOrders
     * @return Result
     */
    public function __invoke(Order $currentOrder, array $reservedOrders): Result
    {
        $intersectOrders = array_filter($reservedOrders, function (Order $order) use ($currentOrder) {
            return $currentOrder->getDates()->intersect($order->getDates());
        });

        if (count($intersectOrders) === 0) {
            return ($this->next)($currentOrder);
        }

        $textWithDates = array_map(function (Order $order): string {
            return "C {$order->getDates()->getBegin()->format('d.m.Y H:i:s')}" .
                " по {$order->getDates()->getEnd()->format('d.m.Y H:i:s')}"
                . ($order->getCustomer()->isVip() ? ' (vip)' : '');
        }, $intersectOrders);

        $message = 'Найдены пересекающиеся даты. ' . implode('. ', $textWithDates);

        return (new Result())->exception(new StopFlow($message));
    }
}