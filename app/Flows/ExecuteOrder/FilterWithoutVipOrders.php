<?php

namespace App\Flows\ExecuteOrder;

use App\Flows\AbstractStep;
use App\Flows\Result;
use App\Models\Order;

class FilterWithoutVipOrders extends AbstractStep
{
    /**
     * @param Order $currentOrder
     * @param Order[] $reservedOrders
     * @return Result
     */
    public function __invoke(Order $currentOrder, array $reservedOrders): Result
    {
        $ordersWithoutVipCustomer = array_filter($reservedOrders, function (Order $order) {
            return $order->getCustomer()->isVip();
        });

        return ($this->next)($currentOrder, $ordersWithoutVipCustomer);
    }
}