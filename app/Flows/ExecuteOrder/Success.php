<?php

namespace App\Flows\ExecuteOrder;

use App\BusinessOperators\WorkingHoursCalculator;
use App\Flows\AbstractStep;
use App\Flows\Result;
use App\Models\Order;

class Success extends AbstractStep
{
    private WorkingHoursCalculator $workingHoursCalculator;

    public function __construct(WorkingHoursCalculator $workingHoursCalculator)
    {
        $this->workingHoursCalculator = $workingHoursCalculator;
    }

    public function __invoke(Order $currentOrder): Result
    {
        return (new Result())
            ->message('Успешно зарезервировано. Количество часов: ' .
                $this->workingHoursCalculator->calculate(
                    $currentOrder->getDates()->getBegin(),
                    $currentOrder->getDates()->getEnd()
                )
            )
            ->success();
    }
}