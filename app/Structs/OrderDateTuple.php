<?php

namespace App\Structs;

use DateTime;

class OrderDateTuple
{
    private DateTime $begin;
    private DateTime $end;

    /**
     * Минуты и секунды должны быть обнулены
     *
     * @param DateTime $begin
     * @param DateTime $end
     */
    public function __construct(DateTime $begin, DateTime $end)
    {
        $this->begin = $begin;
        $this->end = $end;
    }

    public function getBegin(): DateTime
    {
        return $this->begin;
    }

    public function getEnd(): DateTime
    {
        return $this->end;
    }

    public function intersect(self $tuple): bool
    {
        $currentBegin = $this->begin->getTimestamp();
        $currentEnd = $this->end->getTimestamp();

        $receivedBegin = $tuple->getBegin()->getTimestamp();
        $receivedEnd = $tuple->getEnd()->getTimestamp();

        return ($currentBegin < $receivedEnd && $currentEnd > $receivedBegin);
    }
}