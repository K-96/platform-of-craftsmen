#!/usr/bin/make
# Makefile readme (ru): <http://linux.yaroslavl.ru/docs/prog/gnu_make_3-79_russian_manual.html>
# Makefile readme (en): <https://www.gnu.org/software/make/manual/html_node/index.html#SEC_Contents>

NAME = platform-of-craftsmen
REGISTRY_HOST = registry.gitlab.com
REGISTRY_PATH = k-96/$(NAME)/
LATEST = latest
ROOT=.
DOCKER=$(ROOT)/docker

# This will output the help for each task. thanks to https://marmelab.com/blog/2016/02/29/auto-documented-makefile.html
help: ## Show this help
	@awk 'BEGIN {FS = ":.*?## "} /^[a-zA-Z_-]+:.*?## / {printf "  \033[36m%-18s\033[0m %s\n", $$1, $$2}' $(MAKEFILE_LIST)

# Build

IMAGE = $(REGISTRY_HOST)/$(REGISTRY_PATH)$(NAME)

build-first: ## Build image with service. Example: $ make build-crud VERSION='0.1'
	docker build \
		--tag "$(NAME):$(VERSION)" \
		--tag "$(NAME):$(LATEST)" \
		--tag "$(IMAGE):$(VERSION)" \
		--tag "$(IMAGE):$(LATEST)" \
		-f $(DOCKER)/Dockerfile ./;
build: ## Build image with service with cache. Example: $ make build-crud VERSION='0.1'
	docker build \
		--cache-from "$(IMAGE):$(LATEST)" \
		--tag "$(NAME):$(VERSION)" \
		--tag "$(NAME):$(LATEST)" \
		--tag "$(IMAGE):$(VERSION)" \
		--tag "$(IMAGE):$(LATEST)" \
		-f $(DOCKER)/Dockerfile ./;
build-dev: ## Build image with service. Example: $ make build-crud VERSION='0.1'
	docker build \
		--tag "$(NAME):$(VERSION)" \
		--tag "$(NAME):$(LATEST)" \
		--tag "$(IMAGE):$(VERSION)" \
		--tag "$(IMAGE):$(LATEST)" \
		-f $(DOCKER)/DEV_DEP.Dockerfile ./;

# Deploy

install: build-dev ## $ make install VERSION='0.1'

up:
	docker-compose up -d

down:
	docker-compose down

tests: up
	docker-compose exec app php vendor/bin/codecept run unit

acceptance-tests: up
	docker-compose exec app php vendor/bin/codecept run api